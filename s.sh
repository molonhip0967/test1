#!/bin/sh

#This is an example you can edit and use
#There are numerous parameters you can set, please check Help and Examples folder

export GPU_MAX_HEAP_SIZE=100
export GPU_MAX_USE_SYNC_OBJECTS=1
export GPU_SINGLE_ALLOC_PERCENT=100
export GPU_MAX_ALLOC_PERCENT=100
export GPU_MAX_SINGLE_ALLOC_PERCENT=100
export GPU_ENABLE_LARGE_ALLOCATION=100
export GPU_MAX_WORKGROUP_SIZE=1024

./srb --algorithm cryptonight_gpu --pool de.conceal.herominers.com:1115 --wallet ccx7fg9RAYoeWmxereymWEiHxUAatnMMcLL3PDcCMMnThsNReoZwtEPXtznxBsofFP8JB32YYBmtwLdoEirjAbYo4DBZj38Z8G --gpu-boost 3
